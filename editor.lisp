(in-package :croatoan)

(defclass cursor ()
  ((line :initarg :line
         :initform 0
         :accessor cursor-line
         :type (integer 0))
   (column :initarg :column
           :initform 0
           :accessor cursor-column
           :type (integer 0))
   (content :initarg :content
            :initform #\nul
            :accessor cursor-content
            :type 'character
            :documentation "Like a register for the actual char the cursor lies over.")))

(defmacro set-cursor-position (editor x y)
  `(setf (cursor-line (editor-cursor ,editor)) ,y
         (cursor-column (editor-cursor ,editor)) ,x))

(defclass editor ()
  ((content :initarg :content
            :initform (make-array 0 :element-type 'string
                                    :adjustable t
                                    :fill-pointer t)
            :accessor editor-content
            :type (simple-array string (*)))
   (cursor :initarg :cursor
           :initform (make-instance 'cursor :line 0 :column 0)
           :accessor editor-cursor
           :type cursor)
   (mode :initarg :mode
         :initform :normal
         :accessor editor-mode
         :type (member :normal :insert))
   (num-register :initarg :num-register
                 :initform 0
                 :accessor num-register
                 :type (integer 0))
   (command-buffer :initarg :command-buffer
                   :initform (make-array 0 :element-type 'character
                                           :adjustable t)
                   :accessor editor-command-buffer
                   :type string)))

;;; This will go to utils.lisp in the future.
(defun make-adjustable-string ()
  "One of the utility functions."
  (make-array 0 :element-type 'character
                :adjustable t
                :fill-pointer t))

;; Maybe it would make sense defining a macro DEFINE-EDITOR-INSTRUCTION.
;; So we can let it automatically generate after methods for every instruction method.
;; Even better would be letting the entry function being defined by the instructions
;; defined before.

;;; Here a nice property test could be used:
;;; After deleting one char the new string must be exactly one lesser in length.
;;; But not when we deal with an empty string.

(defmethod delete-char-at-position ((old-string string) (position integer)
                                    &aux (length (length old-string)))
  "For now this is called when typing '(#\x) in normal mode."
  (if (str:emptyp old-string)
      ;; It stays the same object.
      old-string
      (progn
        (check-type position (integer 0))
        (assert (< position length))
        (let ((new-string (make-adjustable-string)))
          (symbol-macrolet ((str[i] (char str i)))
            (if (= 0 position)
                (loop for i from 1 below length
                      do (vector-push-extend str[i] new-string)
                      finally (return-from delete-char-at-position new-string))
                (progn
                  (loop for chr across old-string
                        for i below position
                        do (vector-push-extend chr new-string))
                  (loop for i from (1+ position) below length
                        do (vector-push-extend str[i] new-string)
                        finally (return-from delete-char-at-position new-string)))))))))

;;; Instructions that change the cursor position.

(defmethod jump-to-origin ((editor editor))
  "This is called when typing '(#\g #\g) in normal mode."
  (setf (cursor-line (editor-cursor editor)) 0
        (cursor-column (editor-cursor editor)) 0))

(defmethod jump-to-beginning-of-line ((editor editor))
  "This is called when typing '(#\0) in normal mode."
  (setf (cursor-column (editor-cursor editor)) 0))

(defmethod jump-to-end-of-line ((editor editor))
  "This is called when typing '(#\$) in normal mode."
  (setf (cursor-column (editor-cursor editor)) (1- (length ))))

(defmethod jump-to-last-line ((editor editor))
  "This is called when typing '(#\G) in normal mode."
  (set-cursor-position editor 0 (1- (length (editor-content)))))

(defun start-editor ()
  (with-screen (screen :input-echoing nil :input-blocking t :enable-colors t)
    (let ((editor (make-instance 'editor)))
      (symbol-macrolet ((mode (editor-mode editor)))
        (clear screen)
        (move screen 0 0)
        (refresh screen)
        (setf (color-pair screen) '(:yellow :red)
              (attributes screen) '(:bold))
        (event-case (screen event)
          ())

        (#\Esc (setf (editor-mode editor) :normal))
        (otherwise (princ event screen)
                   (refresh screen)))))
